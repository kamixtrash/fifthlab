package secondTaskTests;

import org.junit.Test;
import secondTask.ProxyTask;

import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

public class SecondTaskTest {

    @Test
    public void proxyCheck() {
        Map proxyInstance = (Map) Proxy.newProxyInstance(
                SecondTaskTest.class.getClassLoader(),
                new Class[]{Map.class},
                new ProxyTask(new HashMap<>()));
        System.out.println(proxyInstance.size());
    }
}
