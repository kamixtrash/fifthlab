package firstTaskTests;

import firstTask.Order;
import firstTask.OrderCreator;
import firstTask.Product;
import firstTask.User;
import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FirstTaskTest {

    @Test
    public void createOrder() throws NoSuchFieldException, InstantiationException, IllegalAccessException {
        User user = new User("Foo", "Bar", "Somewhere");
        Product product = new Product("Something", 1);
        Order order = (Order) new OrderCreator().merge(user, product, Order.class);
        System.out.println(order);
        assertEquals("Order{firstName='Foo', lastName='Bar', address='Somewhere', productName='Something', color=1}", order.toString());
    }
}
