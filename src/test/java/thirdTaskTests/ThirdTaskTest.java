package thirdTaskTests;

import org.junit.Test;
import thirdTask.Validator;
import thirdTask.model.Character;

public class ThirdTaskTest {

    @Test
    public void validatorTest() {
        Character character = new Character("foo", null, "bar", "", 100, 200);
        Validator validator = new Validator();
        try {
            validator.check(character);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
