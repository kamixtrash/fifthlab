package thirdTask.model;

import thirdTask.annotations.*;

public class Character {
    @MaxLength
    @MinLength
    private String name;
    @NotNull
    private String race;
    private String charClass;
    @NotEmpty
    private String specialization;
    @Max
    @Min
    private Integer level;
    @Max(value = 100)
    private Integer amountOfItemsInInventory;

    public Character(String name, String race, String charClass, String specialization, Integer level, Integer amountOfItemsInInventory) {
        this.name = name;
        this.race = race;
        this.charClass = charClass;
        this.specialization = specialization;
        this.level = level;
        this.amountOfItemsInInventory = amountOfItemsInInventory;
    }
}
