package thirdTask;

import thirdTask.annotations.*;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

public class Validator {
    public void check(Object o) throws Exception {
        Class<?> clazz = o.getClass();
        for (Field field : clazz.getDeclaredFields()) {
            field.setAccessible(true);
            if (field.isAnnotationPresent(Max.class)) {
                Max annotation = field.getAnnotation(Max.class);
                int max = annotation.value();
                Integer value = (Integer) field.get(o);
                if (value > max) {
                    throw new IllegalStateException(field.getName()
                            + " value should be below " + max);
                }
            }
            if (field.isAnnotationPresent(Min.class)) {
                Min annotation = field.getAnnotation(Min.class);
                int min = annotation.value();
                Integer value = (Integer) field.get(o);
                if (value < min) {
                    throw new IllegalStateException(field.getName()
                            + " value should be higher than " + min);
                }
            }
            if (field.isAnnotationPresent(MaxLength.class)) {
                MaxLength annotation = field.getAnnotation(MaxLength.class);
                int maxLength = annotation.value();
                String value = field.get(o).toString();
                if (value.length() > maxLength) {
                    throw new IllegalStateException(field.getName()
                            + " length should be below " + maxLength);
                }
            }
            if (field.isAnnotationPresent(MinLength.class)) {
                MinLength annotation = field.getAnnotation(MinLength.class);
                int minLength = annotation.value();
                String value = field.get(o).toString();
                if (value.length() < minLength) {
                    throw new IllegalStateException(field.getName()
                            + " length should be bigger than " + minLength);
                }
            }
            if (field.isAnnotationPresent(NotEmpty.class)) {
                String value = field.get(o).toString();
                if (value.isEmpty()) {
                    throw new IllegalStateException(field.getName()
                            + " can't be empty.");
                }
            }
            if (field.isAnnotationPresent(NotNull.class)) {
                Object value = field.get(o);
                try {
                    value.toString();
                } catch (NullPointerException e) {
                    throw new IllegalStateException(field.getName()
                            + " can't be null.");

                }
            }
        }
    }
}
