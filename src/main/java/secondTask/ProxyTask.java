package secondTask;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class ProxyTask implements InvocationHandler {
    private final Object delegate;

    public ProxyTask(Object delegate) {
        this.delegate = delegate;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("Executing method: " + method.getName());
        long startTime = System.currentTimeMillis();
        Object result = method.invoke(delegate, args);
        long endTime = System.currentTimeMillis();
        System.out.println("Time for execution: " + (startTime - endTime));
        return result;
    }
}
