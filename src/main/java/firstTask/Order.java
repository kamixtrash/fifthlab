package firstTask;

public class Order {
    private String firstName;
    private String lastName;
    private String address;
    private String productName;
    private Integer color;

    public Order(String firstName, String lastName, String address, String productName, Integer color) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.productName = productName;
        this.color = color;
    }

    public Order() {}

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getColor() {
        return color;
    }

    public void setColor(Integer color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Order{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", address='" + address + '\'' +
                ", productName='" + productName + '\'' +
                ", color=" + color +
                '}';
    }
}
