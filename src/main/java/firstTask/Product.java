package firstTask;

public class Product {
    private String productName;
    private Integer color;
    private Integer amountInStock;
    private Integer favorites;
    private Integer orders;

    public Product(String productName, Integer color) {
        this.productName = productName;
        this.color = color;
    }

    public Product() {}

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getColor() {
        return color;
    }

    public void setColor(Integer color) {
        this.color = color;
    }

    public Integer getAmountInStock() {
        return amountInStock;
    }

    public void setAmountInStock(Integer amountInStock) {
        this.amountInStock = amountInStock;
    }

    public Integer getFavorites() {
        return favorites;
    }

    public void setFavorites(Integer favorites) {
        this.favorites = favorites;
    }

    public Integer getOrders() {
        return orders;
    }

    public void setOrders(Integer orders) {
        this.orders = orders;
    }

    @Override
    public String toString() {
        return "Product{" +
                "productName='" + productName + '\'' +
                ", color=" + color +
                ", amountInStock=" + amountInStock +
                ", favorites=" + favorites +
                ", orders=" + orders +
                '}';
    }
}
