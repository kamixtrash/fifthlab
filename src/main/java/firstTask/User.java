package firstTask;

public class User {
    private String firstName;
    private String lastName;
    private String country;
    private String address;
    private Integer age;
    private Integer amountOfOrders;
    private Integer amountOfPurchases;

    public User(String firstName, String lastName, String address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
    }

    public User() {}

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getAmountOfOrders() {
        return amountOfOrders;
    }

    public void setAmountOfOrders(Integer amountOfOrders) {
        this.amountOfOrders = amountOfOrders;
    }

    public Integer getAmountOfPurchases() {
        return amountOfPurchases;
    }

    public void setAmountOfPurchases(Integer amountOfPurchases) {
        this.amountOfPurchases = amountOfPurchases;
    }

    @Override
    public String toString() {
        return "User{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", country='" + country + '\'' +
                ", address='" + address + '\'' +
                ", age=" + age +
                ", amountOfOrders=" + amountOfOrders +
                ", amountOfPurchases=" + amountOfPurchases +
                '}';
    }
}
