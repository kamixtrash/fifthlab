package firstTask;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.stream.Collectors;

public class OrderCreator {
    @SuppressWarnings("unchecked")
    public <T> T merge(T firstObject, T secondObject, Class<?> mergedClass) throws InstantiationException, IllegalAccessException, NoSuchFieldException {

        Object mergedObject = mergedClass.newInstance();
        Field[] finalObjectFields = mergedClass.getDeclaredFields();
        Field[] firstObjectFields = firstObject.getClass().getDeclaredFields();
        Field[] secondObjectFields = secondObject.getClass().getDeclaredFields();

        HashMap<String, Object> values = new HashMap<>();

        for (Field field : firstObjectFields) {
            field.setAccessible(true);
            values.put(field.getName(), field.get(firstObject));
        }

        for (Field field : secondObjectFields) {
            field.setAccessible(true);
            values.put(field.getName(), field.get(secondObject));
        }

        for (Field field : finalObjectFields) {
            field.setAccessible(true);
            values.forEach((name, value) -> {
                if (name.equals(field.getName())) {
                    try {
                        field.set(mergedObject, value);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        return (T) mergedObject;
    }
}
